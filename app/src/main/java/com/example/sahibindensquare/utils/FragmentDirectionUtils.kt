package com.example.sahibindensquare.utils

import com.example.sahibindensquare.data.api.entity.Venue
import com.example.sahibindensquare.modules.search.list.SearchListFragmentDirections

/**
 * Used to reduce boilerplate code
 */
class FragmentDirectionUtils {

    companion object {

        fun listToDetail(venue: Venue): SearchListFragmentDirections.ActionListToDetail{
            val action: SearchListFragmentDirections.ActionListToDetail = SearchListFragmentDirections.ActionListToDetail()
            action.stringArgVenueDetail = venue
            return action
        }
    }

}