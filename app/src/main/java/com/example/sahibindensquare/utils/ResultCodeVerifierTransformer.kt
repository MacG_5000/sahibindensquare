package com.example.sahibindensquare.utils

import androidx.lifecycle.MutableLiveData
import com.example.sahibindensquare.base.model.ApiResult
import com.example.sahibindensquare.base.model.BaseApiResponse
import com.example.sahibindensquare.base.model.BaseThrowable
import com.example.sahibindensquare.base.model.State
import io.reactivex.Single
import io.reactivex.SingleTransformer

/**
 * Transformer class for reducing boilerplate code in ViewModels, Repositories etc.
 * Handles transformation of response from api, if it's an error, base class will handle it.
 * If success, ViewModel will handle the result.
 *
 */
class ResultCodeVerifierTransformer<T>(private val state: MutableLiveData<State>,
                                       private val isResultNullable: Boolean = false,
                                       private val throwable: BaseThrowable = BaseThrowable()) : SingleTransformer<BaseApiResponse<T>, ApiResult<T>> {
    override fun apply(tSingle: Single<BaseApiResponse<T>>): Single<ApiResult<T>> {
        return (tSingle
                .doOnSubscribe { state.value = (State.loading(Pair(true, true))) }
                .doFinally { state.value = (State.loading(Pair(false, true))) }
                .flatMap { response ->
                    if (response.isSuccessful) {
                        state.postValue(State.success())
                        if (response.response != null) {
                            Single.just(ApiResult.success(response.response))
                        } else if (!isResultNullable)
                            Single.error(BaseThrowable(
                                    finishActivity = throwable.finishActivity,
                                    retryListener = throwable.retryListener))
                        else
                            Single.just(ApiResult.success(null))
                    } else {
                        Single.error(response.meta?.errorDetail?.let {
                            BaseThrowable(uiMessage = it) }?: BaseThrowable())
                    }
                }
                .onErrorReturn { e ->
                    e.printStackTrace()
                    if (e is BaseThrowable) {
                        state.value = (State.error(e))
                    } else
                        state.value = (State.error(BaseThrowable(e,
                                finishActivity = throwable.finishActivity,
                                retryListener = throwable.retryListener)))
                    ApiResult.error()
                })
    }
}