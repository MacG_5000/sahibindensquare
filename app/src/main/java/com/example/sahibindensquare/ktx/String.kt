package com.example.sahibindensquare.ktx

import android.content.Context
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan
import androidx.annotation.ColorRes
import com.example.sahibindensquare.ui.ColoredUnderlineSpan
import java.util.*

/**
 * Makes a substring of a string bold.
 * @reciever-> Full text
 * @param colorText Text you want to make colered
 * @param colorResId Text you want to make which color
 * @local language local -> default
 * @return String with colered substring
 */
fun String.makeColorSection(context: Context, colorText: String?, @ColorRes colorResId: Int,
                            locale: Locale = Locale.getDefault(), isUnderlined: Boolean? = false): SpannableStringBuilder {
    val builder = SpannableStringBuilder()
    if (colorText?.isNotEmpty() == true && colorText.trim { it <= ' ' } != "") {
        //for counting start/end indexes
        val testText = this.toLowerCase(locale)
        val testTextToColor = colorText.toLowerCase(locale)
        val startingIndex = testText.indexOf(testTextToColor)
        val endingIndex = startingIndex + testTextToColor.length
        //for counting start/end indexes
        if (startingIndex < 0 || endingIndex < 0) {
            return builder.append(this)
        } else if (startingIndex >= 0 && endingIndex >= 0) {
            builder.append(this)
            builder.setSpan(ForegroundColorSpan(context.color(colorResId)), startingIndex, endingIndex, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            isUnderlined?.let {
                builder.setSpan(ColoredUnderlineSpan(context.color(colorResId)), startingIndex, endingIndex, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            }
        }
    } else {
        return builder.append(this)
    }
    return builder
}