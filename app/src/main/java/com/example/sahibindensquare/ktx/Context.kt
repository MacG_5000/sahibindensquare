package com.example.sahibindensquare.ktx

import android.app.Activity
import android.content.Context
import android.content.res.Resources
import android.view.LayoutInflater
import androidx.annotation.ColorRes
import androidx.annotation.StringRes

fun Context.asString(p0: Any?): String? = when (p0) {
    is String -> p0
    is @StringRes Int -> resources.getString(p0)
    else -> null
}

fun Context.inflater(): LayoutInflater = if (this is Activity) this.layoutInflater else LayoutInflater.from(this)

fun Context.color(@ColorRes colorResId: Int, theme: Resources.Theme? = null): Int =
        //if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) resources.getColor(colorResId, theme) else
        // this gives an error due to AndroidStudio bug. All the  .idea/ ,build/, app/build directories need to be deleted.
        //Not needed for this project.
            resources.getColor(colorResId)
