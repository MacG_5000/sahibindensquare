package com.example.sahibindensquare.ktx

import android.app.Activity
import com.example.sahibindensquare.R
import com.example.sahibindensquare.ui.dialogs.base.IDialogInterface
import com.example.sahibindensquare.ui.dialogs.generic.GenericDialog

/** Info Dialog *
 * @param message : @nonnull message
 * @param title: @nullable title if null -> visibility gone
 * @param messageGravity: message gravity -> predefined left
 * @param positiveListener: @nullable  if null -> dialog.dissmiss
 * @param positiveButtonText: @nullable if null -> r.string.ok
 */
fun Activity?.showInfoDialog(message: Any,
                             positiveButtonText: Any? = null, positiveListener: ((dialogPos: IDialogInterface) -> Unit)? = null): GenericDialog? {
    if (this?.isFinishing != false) return null
    return GenericDialog.Builder(this)
            .setPositiveButton(positiveButtonText, positiveListener = positiveListener)
            .setMessage(asString(message)!!)
            .show()
}

/** Retry Dialog *
 * @param message : @nonnull message
 * @param title: @nullable title if null -> visibility gone
 * @param positiveListener: @nullable  if null -> dialog.dissmiss
 * @param negativeListener: @nullable  if null -> dialog.dissmiss
 */
fun Activity?.showRetryDialog(message: Any,
                              positiveListener: ((dialogPos: IDialogInterface) -> Unit)?,
                              negativeListener: ((dialogNeg: IDialogInterface) -> Unit)?): GenericDialog? {
    if (this?.isFinishing != false) return null
    return GenericDialog.Builder(this)
            .setPositiveButton(R.string.retry, positiveListener = positiveListener)
            .setNegativeButton(R.string.cancel, negativeListener = negativeListener)
            .setMessage(asString(message)!!)
            .show()
}

