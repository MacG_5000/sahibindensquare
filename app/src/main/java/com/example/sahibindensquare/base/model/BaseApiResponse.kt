package com.example.sahibindensquare.base.model

import com.example.sahibindensquare.data.api.entity.Meta

class BaseApiResponse<out T> {
    companion object {
        private const val API_RESPONSE_CODE_SUCCESS = 200
    }

    val meta: Meta? = null
    val response: T? = null

    val isSuccessful: Boolean
        get() = this.meta?.code == API_RESPONSE_CODE_SUCCESS

}