package com.example.sahibindensquare.base.model

import com.example.sahibindensquare.R
import com.example.sahibindensquare.ui.dialogs.base.IDialogInterface

/**
 * This class is used for all error handling across the application.
 * When a Fragment receives a BaseThrowable, it can decide to
 * show a message, stop loading indicators, finishing activity or going back a fragment,
 * or retrying the causing action.
 */
open class BaseThrowable : Throwable {

    var showMessage: Boolean = true
        private set
    var stopLoading: Boolean = true
        private set
    var finishActivity: Boolean = false
        private set
    var uiMessage: Any
        get() = if (-1 == field || "" == field) noneEmptyMessage else field
        private set

    var retryListener: ((dialog: IDialogInterface) -> Unit)? = null
        private set

    protected open var noneEmptyMessage = R.string.error_generic

    constructor(cause: Throwable, finishActivity: Boolean = false, uiMessage: Any = "",
                retryListener: ((dialog: IDialogInterface) -> Unit)? = null) : super(cause) {
        this.showMessage = true
        this.stopLoading = true
        this.finishActivity = finishActivity
        this.uiMessage = uiMessage
        this.retryListener = retryListener
    }

    constructor(finishActivity: Boolean = false, uiMessage: Any = "",
                retryListener: ((dialog: IDialogInterface) -> Unit)? = null) : super(uiMessage.toString()) {
        this.showMessage = true
        this.stopLoading = true
        this.finishActivity = finishActivity
        this.uiMessage = uiMessage
        this.retryListener = retryListener
    }
}