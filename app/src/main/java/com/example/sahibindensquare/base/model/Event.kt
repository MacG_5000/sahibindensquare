package com.example.sahibindensquare.base.model

/**
 * Event is used so that an Observer consumes whatever it's observing only once.
 */
open class Event<out T>(private val content: T) {

    var hasBeenHandled = false
        private set // Allow external read but not write

    /**
     * Returns the content and prevents its use again.
     */
    fun getContentIfNotHandled(): T? {
        if (hasBeenHandled) {
           return null
        } else {
            hasBeenHandled = true
            return content
        }
    }

    /**
     * Returns the content, even if it's already been handled.
     */
    fun peekContent(): T = content
}