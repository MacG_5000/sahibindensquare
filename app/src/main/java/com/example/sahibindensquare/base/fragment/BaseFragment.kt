package com.example.sahibindensquare.base.fragment

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.example.sahibindensquare.base.interfaces.IBaseView
import com.example.sahibindensquare.base.interfaces.ILoadingIndicator
import com.example.sahibindensquare.base.model.BaseThrowable
import com.example.sahibindensquare.base.model.Status
import com.example.sahibindensquare.base.viewmodel.BaseViewModel
import com.example.sahibindensquare.ktx.showInfoDialog
import com.example.sahibindensquare.ktx.showRetryDialog
import com.example.sahibindensquare.ui.dialogs.LoadingDialog
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.AndroidSupportInjection
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

/**
 * Base fragment handles showing loading indicators and error handling.
 * When used with [ResultCodeVerifierTransformer], all the fragments need to handle is
 * when data is retrieved from the apis successfully.
 */
open class BaseFragment : Fragment(), IBaseView, ILoadingIndicator, HasSupportFragmentInjector {

    @Inject
    lateinit var childFragmentInjector: DispatchingAndroidInjector<Fragment>
    private lateinit var loadingDialog: LoadingDialog
    val isActive = isAdded

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        loadingDialog = LoadingDialog(requireActivity())
    }

    override fun supportFragmentInjector(): AndroidInjector<Fragment> = childFragmentInjector

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onError(t: BaseThrowable) {
        if (!isAdded) return

        if (t.finishActivity) {
            setLoadingIndicator(false, false)
            activity.showInfoDialog(t.uiMessage, positiveListener = { dialog ->
                dialog.dismiss()
                activity?.onBackPressed()
            })
            return
        }

        if (t.retryListener != null) {
            setLoadingIndicator(false, false)
            activity.showRetryDialog(t.uiMessage, positiveListener = t.retryListener,
                    negativeListener = { dialog ->
                        dialog.dismiss()
                        activity?.onBackPressed()
                    })
            return
        }

        if (t.stopLoading) {
            setLoadingIndicator(false, true)
        }
        if (t.showMessage) {
            activity.showInfoDialog(t.uiMessage)
        }
    }

    override fun setLoadingIndicator(loader: Boolean, rootView: Boolean) {
        if (!isAdded) return
        loadingDialog.apply {
            if (loader) {
                if (!isShowing) show()
            } else
                if (isShowing) dismiss()
        }
        view?.isVisible = rootView
    }

    override fun setLoadingIndicator(pair: Pair<Boolean, Boolean>) {
        setLoadingIndicator(pair.first, pair.second)
    }

    fun handleState(viewModel: BaseViewModel){
        viewModel.state.observe(viewLifecycleOwner, Observer {
            when(it.status){
                Status.ERROR-> onError(it.error ?: BaseThrowable())
                Status.LOADING -> setLoadingIndicator(it.loadingIndicator ?: Pair(true, true))
                Status.SUCCESS -> Log.d("BaseFragment","Success")

            }
        })
    }
}