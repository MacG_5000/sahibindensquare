package com.example.sahibindensquare.base

import android.annotation.SuppressLint
import android.os.Bundle
import dagger.android.support.DaggerAppCompatActivity

@SuppressLint("Registered")
open class BaseActivity: DaggerAppCompatActivity() {

}