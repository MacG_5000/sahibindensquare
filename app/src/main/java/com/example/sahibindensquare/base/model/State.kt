package com.example.sahibindensquare.base.model


@Suppress("DataClassPrivateConstructor")
data class State private constructor(val status: Status, val error: BaseThrowable?, val loadingIndicator: Pair<Boolean, Boolean>?){
    companion object {
        fun success(): State {
            return State(Status.SUCCESS, null, null)
        }

        fun error(error: BaseThrowable): State {
            return State(Status.ERROR, error, null)
        }

        fun loading(loadingIndicator: Pair<Boolean, Boolean>?): State {
            return State(Status.LOADING, null, loadingIndicator)
        }
    }

}