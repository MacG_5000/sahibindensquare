package com.example.sahibindensquare.base.interfaces

import com.example.sahibindensquare.base.model.BaseThrowable

interface IBaseView {
    fun onError(t: BaseThrowable)
}