package com.example.sahibindensquare.constants

object RequestCodes {
    const val FSQ_CONNECT = 1001
    const val FSQ_TOKEN_EXCHANGE = 1002
}