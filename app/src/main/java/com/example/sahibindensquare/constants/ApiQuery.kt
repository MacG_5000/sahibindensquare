package com.example.sahibindensquare.constants

object ApiQuery {
    const val CLIENT_ID = "client_id"
    const val CLIENT_SECRET = "client_secret"
    const val FSQ_API_VERSION = "v"
    const val LOCATION = "ll"
    const val OAUTH_TOKEN = "oauth_token"

}