package com.example.sahibindensquare.constants

object ApiUrls {

    const val ENDPOINT = "https://api.foursquare.com/v2/"
    const val VENUES = "venues/search"
    const val CHECK_INS = "users/self/checkins"
}