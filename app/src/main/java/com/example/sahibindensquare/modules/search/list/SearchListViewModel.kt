package com.example.sahibindensquare.modules.search.list

import androidx.lifecycle.MutableLiveData
import com.example.sahibindensquare.base.model.ApiResult
import com.example.sahibindensquare.base.model.Event
import com.example.sahibindensquare.base.model.State
import com.example.sahibindensquare.base.viewmodel.BaseViewModel
import com.example.sahibindensquare.constants.Config
import com.example.sahibindensquare.data.UserManager
import com.example.sahibindensquare.data.api.ApiService
import com.example.sahibindensquare.data.api.entity.Venue
import com.example.sahibindensquare.data.api.response.UserCheckinsResponse
import com.example.sahibindensquare.data.api.response.VenuesResponse
import com.example.sahibindensquare.utils.ResultCodeVerifierTransformer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class SearchListViewModel @Inject constructor(private val apiService: ApiService,
                                              private val userManager: UserManager): BaseViewModel(){

    val venuesListener: MutableLiveData<Event<VenuesResponse>> = MutableLiveData()

    fun loadData(){
        /**
         *If the user's logged in, get their checkins, else, get some places' venue list.
         */
        if(userManager.loggedIn)
            loadUserCheckin()
        else
            loadMockVenues()
    }

    /**
     * Usually location of device's current location is passed to such api calls,
     * but I'm using a mock location for simplicity.
     */
    private fun loadMockVenues(){
        subscribe(apiService.getVenues(Config.CLIENT_ID, Config.CLIENT_SECRET,
                Config.FSQ_API_VERSION, "40.7463956,-73.9852992")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    if (state.value != State.loading(Pair(true, false)))
                        state.value = (State.loading(Pair(true, true)))
                }
                .doFinally { state.value = (State.loading(Pair(false, true))) }
                .compose(ResultCodeVerifierTransformer(state = state, isResultNullable = false)) // we don't want a response with null data!
                .subscribe { response: ApiResult<VenuesResponse> ->
                    if (response.isSuccess && response.data != null) {
                        venuesListener.value = Event(response.data)
                    }
                })
    }

    private fun loadUserCheckin(){
        subscribe(apiService.getCheckins(userManager.token, Config.FSQ_API_VERSION)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    if (state.value != State.loading(Pair(true, false)))
                        state.value = (State.loading(Pair(true, true)))
                }
                .doFinally { state.value = (State.loading(Pair(false, true))) }
                .compose(ResultCodeVerifierTransformer(state = state, isResultNullable = false)) // we don't want a response with null data!
                .subscribe { response: ApiResult<UserCheckinsResponse> ->
                    if (response.isSuccess && response.data != null) {
                        //I don't want to make another list view for checkins, so I'll just pass the
                        // venues the user checked in at to the adapter.
                        val list: ArrayList<Venue> = ArrayList()
                        response.data.checkins.items.forEach {
                            it.venue?.let { it1 -> list.add(it1) } //I did not make a UI design if this list is empty.
                        }
                        venuesListener.value = Event(VenuesResponse(list))
                    }
                })
    }
}