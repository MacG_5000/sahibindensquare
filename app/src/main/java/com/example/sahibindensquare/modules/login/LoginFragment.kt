package com.example.sahibindensquare.modules.login

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.sahibindensquare.R
import com.example.sahibindensquare.base.fragment.BaseFragment
import com.example.sahibindensquare.base.model.BaseThrowable
import com.example.sahibindensquare.base.viewmodel.ViewModelFactory
import com.example.sahibindensquare.constants.Config
import com.example.sahibindensquare.constants.RequestCodes
import com.example.sahibindensquare.ktx.asString
import com.example.sahibindensquare.ktx.makeColorSection
import com.example.sahibindensquare.modules.main.MainViewModel
import com.foursquare.android.nativeoauth.FoursquareOAuth
import kotlinx.android.synthetic.main.fragment_login.*
import javax.inject.Inject


class LoginFragment: BaseFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    private lateinit var mainViewModel: MainViewModel

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mainViewModel = ViewModelProviders.of(this, viewModelFactory).get(MainViewModel::class.java)
        with(mainViewModel){
            onLoggedInStatus(isLoggedIn())
            loginListener.observe(requireActivity(), Observer {
                onLoggedInStatus(it)
            })
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btnLogin.setOnClickListener {
            getAccessCode()
        }
        btnLogout.setOnClickListener { mainViewModel.onLogout() }
        val devsNotes = requireContext().getString(R.string.devs_notes)
        with(tvDevelopersNote){
            text = devsNotes.makeColorSection(requireContext(),
                    devsNotes, R.color.lightBlue, isUnderlined = true)
            setOnClickListener { text = requireContext().getString(R.string.notes) }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK){
            when(requestCode){
                RequestCodes.FSQ_CONNECT -> {
                    val codeResponse = FoursquareOAuth.getAuthCodeFromResult(resultCode, data)
                    codeResponse.exception?.let {ex ->
                        displayFsqException(ex)
                    }?: kotlin.run {
                        //access code granted, let's authorize the user now!
                        getToken(codeResponse.code)
                    }
                }

                RequestCodes.FSQ_TOKEN_EXCHANGE -> {
                    val tokenResponse = FoursquareOAuth.getTokenFromResult(resultCode, data)
                    tokenResponse.exception?.let {ex ->
                        displayFsqException(ex)
                    }?: kotlin.run {
                        setLoadingIndicator(false, true)
                        mainViewModel.onLogin(tokenResponse.accessToken)
                    }
                }
            }
        }else if(requestCode == RequestCodes.FSQ_TOKEN_EXCHANGE
                || requestCode == RequestCodes.FSQ_CONNECT){
            setLoadingIndicator(false, true)
        }
    }

    private fun onLoggedInStatus(isLoggedIn: Boolean){
        btnLogin?.isVisible = !isLoggedIn
        btnLogout?.isVisible = isLoggedIn
        if(isLoggedIn){
            tvStatus?.text = requireContext().asString(R.string.status_logged_in)
        }else
            tvStatus?.text = requireContext().asString(R.string.status_logged_out)
    }

    private fun getAccessCode(){
        setLoadingIndicator(true, true)
        val i: Intent = FoursquareOAuth.getConnectIntent(requireContext(), Config.CLIENT_ID)
        startActivityForResult(i, RequestCodes.FSQ_CONNECT)
    }

    private fun getToken(authCode: String){
        val intent = FoursquareOAuth.getTokenExchangeIntent(context, Config.CLIENT_ID, Config.CLIENT_SECRET, authCode)
        startActivityForResult(intent, RequestCodes.FSQ_TOKEN_EXCHANGE)
    }

    private fun displayFsqException(ex: Exception){
        setLoadingIndicator(false, true)
        val message = ex.message ?: ex.localizedMessage
        onError(BaseThrowable(uiMessage = message,
                retryListener = { dialog ->
                    btnLogin.performClick()
                    dialog.dismiss()
                }))
    }

}