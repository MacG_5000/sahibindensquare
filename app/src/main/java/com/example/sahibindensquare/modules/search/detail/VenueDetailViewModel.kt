package com.example.sahibindensquare.modules.search.detail

import androidx.lifecycle.MutableLiveData
import com.example.sahibindensquare.base.viewmodel.BaseViewModel
import com.example.sahibindensquare.data.api.entity.Venue
import javax.inject.Inject

class VenueDetailViewModel @Inject constructor(): BaseViewModel() {

    var venueListener: MutableLiveData<Venue> = MutableLiveData()

}