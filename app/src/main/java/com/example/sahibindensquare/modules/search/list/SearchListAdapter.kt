package com.example.sahibindensquare.modules.search.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.sahibindensquare.R
import com.example.sahibindensquare.data.api.entity.Venue
import kotlinx.android.synthetic.main.list_item_venue.view.*
import javax.inject.Inject

class SearchListAdapter @Inject constructor() : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var list: ArrayList<Venue> = ArrayList()
    var listener: ((Venue) -> Unit)? = null

    fun setData(list: ArrayList<Venue>? ){
        list?.let {
            this.list = it
            notifyDataSetChanged()
        }
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_item_venue, parent, false))
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ViewHolder).bind(list[position], listener)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(venue: Venue, listener: ((Venue) -> Unit)? = null) = with(itemView){
            venue.name?.let { tvName.text = it }
            tvAddress.text = venue.location.address
            root.setOnClickListener { listener?.invoke(venue) }
        }
    }
}