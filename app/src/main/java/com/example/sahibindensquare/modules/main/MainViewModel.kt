package com.example.sahibindensquare.modules.main

import com.example.sahibindensquare.base.viewmodel.BaseViewModel
import com.example.sahibindensquare.data.UserManager
import javax.inject.Inject

/**
 * MainViewModel handles login events, in a big application such as Odamax, Etstur,
 * there are a few places a user may login, so it's best to have a shared login listener
 * between multiple fragments.
 */
class MainViewModel @Inject constructor(private val userManager: UserManager) : BaseViewModel() {

    val loginListener = userManager.loginListener

    fun isLoggedIn(): Boolean{
        return userManager.loggedIn
    }

    fun onLogin(token: String){
        userManager.onLogin(token)
    }

    fun onLogout() = userManager.onLogout()

}