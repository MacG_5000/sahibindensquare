package com.example.sahibindensquare.modules.search.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.sahibindensquare.R
import com.example.sahibindensquare.base.fragment.BaseFragment
import com.example.sahibindensquare.base.viewmodel.ViewModelFactory
import com.example.sahibindensquare.data.api.entity.Venue
import kotlinx.android.synthetic.main.fragment_venue_detail.*
import javax.inject.Inject

class VenueDetailFragment: BaseFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    private lateinit var venueDetailViewModel: VenueDetailViewModel

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        venueDetailViewModel = ViewModelProviders.of(requireActivity(), viewModelFactory).get(VenueDetailViewModel::class.java)
        arguments?.let {
            var venue: Venue? = VenueDetailFragmentArgs.fromBundle(it).stringArgVenueDetail
            venue?.let { vn ->
                venueDetailViewModel.venueListener.value = vn
            }
        }
        with(venueDetailViewModel){
            venueListener.observe(viewLifecycleOwner, Observer {
                /**
                 * In a screen like this, there is usually too many imformation.
                 * Instead of writing all the code in a fragment, we will have multiple layout classes
                 * which handle their relevant information
                 *
                 * VenueDetailTitleLayout class mocks the title information of a Venue.
                 */
                venueDetailTitleLayout.setData(it?.name, it.location.address)
            })
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_venue_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        toolbarLayout.registerToolbarToActivity(requireActivity() as AppCompatActivity)
    }

}