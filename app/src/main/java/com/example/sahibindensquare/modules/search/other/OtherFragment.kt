package com.example.sahibindensquare.modules.search.other

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.sahibindensquare.R
import com.example.sahibindensquare.base.fragment.BaseFragment

/**
 * Not Implemented
 */
class OtherFragment: BaseFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_other, container, false)
    }
}