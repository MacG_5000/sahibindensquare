package com.example.sahibindensquare.modules.search.detail.ui

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import com.example.sahibindensquare.R
import kotlinx.android.synthetic.main.layout_venue_detail_title.view.*


/**
 * In a screen like this, there is usually too many information.
 * Instead of writing all the code in a fragment, and to have a very very long xml layout file, we will have multiple layout classes
 * which handle their relevant information.
 *
 * This class mocks the title information of a Venue.
 */
class VenueDetailTitleLayout @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr)  {

    init {
        View.inflate(context, R.layout.layout_venue_detail_title, this)
    }

    fun setData(name: String?, address: String?){
        tvName.text = name
        tvAddress.text = address
    }
}