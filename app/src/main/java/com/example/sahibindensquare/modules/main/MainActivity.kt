package com.example.sahibindensquare.modules.main

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.example.sahibindensquare.R
import com.example.sahibindensquare.base.BaseActivity
import com.example.sahibindensquare.base.viewmodel.ViewModelFactory
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

@SuppressLint("Registered")
open class MainActivity : BaseActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    private lateinit var mainViewModel: MainViewModel
    private val navController by lazy { findNavController(R.id.mainNavigationFragment) }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mainViewModel = ViewModelProviders.of(this, viewModelFactory).get(MainViewModel::class.java)
        /**
         *I used the new navigation components. It's gone Beta now but I used the Alpha version in this project.
         */
        bottomNavigation.setupWithNavController(navController)
    }
}
