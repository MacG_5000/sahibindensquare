package com.example.sahibindensquare.modules.search.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.sahibindensquare.R
import com.example.sahibindensquare.base.fragment.BaseFragment
import com.example.sahibindensquare.base.viewmodel.ViewModelFactory
import com.example.sahibindensquare.utils.FragmentDirectionUtils
import kotlinx.android.synthetic.main.fragment_list.*
import javax.inject.Inject

class SearchListFragment: BaseFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    @Inject
    lateinit var adapter:SearchListAdapter
    private lateinit var searchListViewModel: SearchListViewModel

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        searchListViewModel = ViewModelProviders.of(requireActivity(), viewModelFactory).get(SearchListViewModel::class.java)
        with(searchListViewModel){
            handleState(this)
            venuesListener.observe(viewLifecycleOwner, Observer {
                adapter.setData(it.getContentIfNotHandled()?.venues)
            })
        }
        searchListViewModel.loadData()
    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        toolbarLayout.registerToolbarToActivity(requireActivity() as AppCompatActivity)
        recyclerView.layoutManager = LinearLayoutManager(requireContext())
        recyclerView.adapter = adapter
        adapter.listener = {
            findNavController().navigate(FragmentDirectionUtils.listToDetail(it))
        }
    }
}