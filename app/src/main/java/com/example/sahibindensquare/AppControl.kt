package com.example.sahibindensquare

import com.example.sahibindensquare.di.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication

/**
 * DaggerApplication handles Dagger related code.
 */
class AppControl: DaggerApplication() {

    companion object {
        lateinit var instance: AppControl
            private set
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
    }

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> = DaggerAppComponent.builder().create(this)

}