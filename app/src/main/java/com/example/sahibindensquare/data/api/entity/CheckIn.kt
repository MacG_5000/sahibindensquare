package com.example.sahibindensquare.data.api.entity

import android.os.Parcel
import android.os.Parcelable

/**
 * Only basic items for simplicity!
 */
data class CheckIn (val id: String? = null,
               val type: String? = null,
               val venue: Venue? = null) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readParcelable(Venue::class.java.classLoader)) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(type)
        parcel.writeParcelable(venue, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<CheckIn> {
        override fun createFromParcel(parcel: Parcel): CheckIn {
            return CheckIn(parcel)
        }

        override fun newArray(size: Int): Array<CheckIn?> {
            return arrayOfNulls(size)
        }
    }
}