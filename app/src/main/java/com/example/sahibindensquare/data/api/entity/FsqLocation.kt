package com.example.sahibindensquare.data.api.entity

import android.os.Parcel
import android.os.Parcelable

data class FsqLocation(var address: String? = null,
                       var crossStreet: String? = null,
                       var lat: Double? = 0.0,
                       var lng: Double? = 0.0,
                       var distance: Int? = 0,
                       var postalCode: String? = null,
                       var cc: String? = null,
                       var city: String? = null,
                       var state: String? = null,
                       var country: String? = null) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readValue(Double::class.java.classLoader) as? Double,
            parcel.readValue(Double::class.java.classLoader) as? Double,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(address)
        parcel.writeString(crossStreet)
        parcel.writeValue(lat)
        parcel.writeValue(lng)
        parcel.writeValue(distance)
        parcel.writeString(postalCode)
        parcel.writeString(cc)
        parcel.writeString(city)
        parcel.writeString(state)
        parcel.writeString(country)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<FsqLocation> {
        override fun createFromParcel(parcel: Parcel): FsqLocation {
            return FsqLocation(parcel)
        }

        override fun newArray(size: Int): Array<FsqLocation?> {
            return arrayOfNulls(size)
        }
    }

}