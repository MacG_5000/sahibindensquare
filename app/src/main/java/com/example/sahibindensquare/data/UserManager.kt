package com.example.sahibindensquare.data

import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Handles user login, logout events.
 */
@Singleton
class UserManager @Inject constructor(private val securePreferences: SecurePreferences) {
    companion object {
        private const val KEY_TOKEN = "tk_data"
    }

    private var tokenFromPreferences: String = securePreferences.getString(KEY_TOKEN) ?: ""

    val loginListener: MutableLiveData<Boolean> = MutableLiveData()

    val loggedIn: Boolean
        get() = !token.isEmpty()

    var token: String = ""
        get() = if (field.isEmpty()) tokenFromPreferences else field
        private set

    fun onLogin(token: String) {
        this.token = token
        securePreferences.put(KEY_TOKEN, token)
        tokenFromPreferences = token
        loginListener.value = true
    }

    fun onLogout() {
        this.token = ""
        securePreferences.put(KEY_TOKEN, "")
        tokenFromPreferences = ""
        loginListener.value = false
    }

}