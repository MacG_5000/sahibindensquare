package com.example.sahibindensquare.data.api

import com.example.sahibindensquare.base.model.BaseApiResponse
import com.example.sahibindensquare.constants.ApiQuery
import com.example.sahibindensquare.constants.ApiUrls
import com.example.sahibindensquare.data.api.response.UserCheckinsResponse
import com.example.sahibindensquare.data.api.response.VenuesResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET(ApiUrls.VENUES)
    fun getVenues(@Query(ApiQuery.CLIENT_ID) id: String,
                  @Query(ApiQuery.CLIENT_SECRET) secret: String,
                  @Query(ApiQuery.FSQ_API_VERSION) v: String,
                  @Query(ApiQuery.LOCATION) location: String): Single<BaseApiResponse<VenuesResponse>>

    @GET(ApiUrls.CHECK_INS)
    fun getCheckins(@Query(ApiQuery.OAUTH_TOKEN) token: String,
                    @Query(ApiQuery.FSQ_API_VERSION) v: String): Single<BaseApiResponse<UserCheckinsResponse>>
}