package com.example.sahibindensquare.data.api.response

import com.example.sahibindensquare.data.api.entity.CheckIns

data class UserCheckinsResponse(val checkins: CheckIns)