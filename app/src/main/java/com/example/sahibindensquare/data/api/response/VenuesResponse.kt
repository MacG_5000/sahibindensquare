package com.example.sahibindensquare.data.api.response

import com.example.sahibindensquare.data.api.entity.Venue

data class VenuesResponse(val venues: ArrayList<Venue>)