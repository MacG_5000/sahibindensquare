package com.example.sahibindensquare.ui

import android.text.TextPaint
import android.text.style.CharacterStyle
import android.text.style.UpdateAppearance

internal class ColoredUnderlineSpan(private val mColor: Int) : CharacterStyle(), UpdateAppearance {

    override fun updateDrawState(tp: TextPaint) {
        try {
            val method = TextPaint::class.java.getMethod(
                    "setUnderlineText",
                    Integer.TYPE,
                    java.lang.Float.TYPE
            )
            method.invoke(tp, mColor, 8.0f)
        } catch (e: Exception) {
            tp.isUnderlineText = true
        }

    }
}