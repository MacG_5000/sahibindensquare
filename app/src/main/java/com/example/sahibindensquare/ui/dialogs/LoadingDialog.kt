package com.example.sahibindensquare.ui.dialogs

import android.content.Context
import android.os.Bundle
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDialog
import com.example.sahibindensquare.R

/**
 * Shows a stupid looking progressbar for simplicity
 */
class LoadingDialog (context: Context) : AppCompatDialog(context)  {

    var isActive: Boolean = false
        private set

    init {
        window?.let {
            it.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        }
        setCanceledOnTouchOutside(false)
        setCancelable(false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_loading)
    }

    override fun dismiss() {
        isActive = false
        super.dismiss()
    }

    override fun show() {
        super.show()
        isActive = true
    }
}