package com.example.sahibindensquare.ui.dialogs.base

interface IDialogInterface {
    fun dismiss()

    interface OnDismissListener {

        fun onDismiss(dialog: IDialogInterface)
    }

    interface OnPositiveButtonClickListener {

        fun onClickPositiveButton(dialog: IDialogInterface)
    }

    interface OnNeutralButtonClickListener {

        fun onClickNeutralButton(dialog: IDialogInterface)
    }

    interface OnNegativeButtonClickListener {

        fun onClickNegativeButton(dialog: IDialogInterface)
    }
}