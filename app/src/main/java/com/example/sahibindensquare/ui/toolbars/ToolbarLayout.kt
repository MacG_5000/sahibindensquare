package com.example.sahibindensquare.ui.toolbars

import android.content.Context
import android.util.AttributeSet
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import com.example.sahibindensquare.R
import com.example.sahibindensquare.ktx.inflater
import kotlinx.android.synthetic.main.layout_toolbar.view.*

/**
 * Our custom Toolbar implementation. We needed this class because we have very different toolbars in our apps,
 * (like a Toolbar where a user can make a textfield search) and we really enjoy writing our own View components.
 */
class ToolbarLayout @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) : LinearLayout(context, attrs, defStyleAttr), IToolbarLayout {

    /**
     * Title
     */
    private var _title: String? = null
    var title: String?
        get() = _title
        set(value) {
            _title = value
            applyTitleText()
        }

    private fun applyTitleText() {
        title?.let { tvTitle.text = it }
    }

    private var _showBackButton: Boolean = true
    var showBackButton: Boolean
    get()= _showBackButton
    set(value) {
        _showBackButton = value
        applyShowBackButton()
    }

    private fun applyShowBackButton(){
        rlLeft.isVisible = showBackButton
    }

    init {
        context.inflater().inflate(R.layout.layout_toolbar, this)
        initAttrs(attrs)
    }

    private fun initAttrs(attrs: AttributeSet?) {
        attrs?.let {
            val typedArray = context.theme.obtainStyledAttributes(
                    attrs, R.styleable.toolbar_layout, 0, 0
            )
            try {
                title = typedArray.getString(R.styleable.toolbar_layout_tl_title)
                showBackButton = typedArray.getBoolean(R.styleable.toolbar_layout_tl_show_back_button, _showBackButton)
            } finally {
                typedArray.recycle()
            }
        }
    }

    override fun registerToolbarToActivity(activity: AppCompatActivity?) {
        activity?.let {
            it.setSupportActionBar(dasToolbar)
            it.supportActionBar?.setDisplayShowTitleEnabled(false)
            it.supportActionBar?.setDisplayShowCustomEnabled(true)
            rlLeft.setOnClickListener {
                activity.onBackPressed()
            }
        }
    }
}