package com.example.sahibindensquare.ui.dialogs.base

open class BaseDialogParam(
        val positiveButtonText: String?,
        val negativeButtonText: String?,
        val positiveButtonClickListener: ((IDialogInterface) -> Unit)?,
        val negativeButtonClickListener: ((IDialogInterface) -> Unit)?)
