package com.example.sahibindensquare.ui.dialogs.generic

import com.example.sahibindensquare.ui.dialogs.base.BaseDialogParam
import com.example.sahibindensquare.ui.dialogs.base.IDialogInterface

class GenericDialogParam private constructor(val message: String?,
                                             positiveButtonText: String?,
                                             negativeButtonText: String?,
                                             positiveButtonClickListener: ((IDialogInterface) -> Unit)?,
                                             negativeButtonClickListener: ((IDialogInterface) -> Unit)? )
    : BaseDialogParam(positiveButtonText, negativeButtonText, positiveButtonClickListener, negativeButtonClickListener) {
    constructor(b: GenericDialog.Builder) : this(b.message, b.positiveButtonText, b.negativeButtonText,
            b.positiveButtonClickListener, b.negativeButtonClickListener)
}