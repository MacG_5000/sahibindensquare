package com.example.sahibindensquare.ui.toolbars

import androidx.appcompat.app.AppCompatActivity

interface IToolbarLayout {
    fun registerToolbarToActivity(activity: AppCompatActivity?)
}