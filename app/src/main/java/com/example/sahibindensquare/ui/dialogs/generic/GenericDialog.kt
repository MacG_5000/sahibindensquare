package com.example.sahibindensquare.ui.dialogs.generic

import android.content.Context
import android.os.Bundle
import androidx.core.view.isVisible
import com.example.sahibindensquare.R
import com.example.sahibindensquare.ktx.asString
import com.example.sahibindensquare.ui.dialogs.base.BaseDialog
import com.example.sahibindensquare.ui.dialogs.base.IDialogInterface
import kotlinx.android.synthetic.main.dialog_generic.*

/**
 * We can create custom dialogs across the application with this dialog
 */
class GenericDialog private constructor(context: Context, theme: Int) : BaseDialog(context, theme), IDialogInterface {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_generic)
    }

    private fun apply(p: GenericDialogParam) {
        /** message */
        p.message?.let { message ->
            tvMessage.apply {
                isVisible = true
                text = message
            }
        } ?: kotlin.run { tvMessage.isVisible = false }

        /** positive button is always visible **/
        p.positiveButtonText?.let { btnPositive.text = it }
                ?: kotlin.run { btnPositive.text = context.resources.getString(R.string.ok) }
        p.positiveButtonClickListener?.let { t -> btnPositive.setOnClickListener { t(this) } }
                ?: btnPositive.setOnClickListener { dismiss() }

        /** negative button */
        if (p.negativeButtonText != null || p.negativeButtonClickListener != null) {
            btnNegative.isVisible = true
            p.negativeButtonText?.let { btnNegative.text = it }
                    ?: kotlin.run { btnNegative.text = context.resources.getString(R.string.cancel) }
            p.negativeButtonClickListener?.let { t -> btnNegative.setOnClickListener { t(this) } }
                    ?: btnNegative.setOnClickListener { dismiss() }
        } else {
            btnNegative.isVisible = false
        }
    }

    class Builder(val context: Context) {
        var message: String? = null
            private set
        var positiveButtonText: String? = null
            private set
        var negativeButtonText: String? = null
            private set
        var positiveButtonClickListener: ((IDialogInterface) -> Unit)? = null
            private set
        var negativeButtonClickListener: ((IDialogInterface) -> Unit)? = null
            private set

        fun setMessage(text: String): Builder = this.apply {
            this.message = text
        }

        fun setPositiveButton(
                text: Any? = null,
                positiveListener: ((dialogPos: IDialogInterface) -> Unit)? = null
        ) = this.apply {
            this.positiveButtonText = context.asString(text)
            this.positiveButtonClickListener = positiveListener
        }

        fun setNegativeButton(
                text: Any? = null,
                negativeListener: ((dialogNeg: IDialogInterface) -> Unit)? = null
        ) = this.apply {
            this.negativeButtonText = context.asString(text)
            this.negativeButtonClickListener = negativeListener
        }

        fun createDialog(): GenericDialog = GenericDialog(context, R.style.Widget_Sahibindensqr_GenericDialog).apply {
            /**
             * There's a configirutaion bug here -> Unresolved reference: create
             * This code runs perfectly in all other applications
             */
            //create()
            apply(GenericDialogParam(this@Builder))
        }

        fun show() = createDialog().apply { show() }
    }
}