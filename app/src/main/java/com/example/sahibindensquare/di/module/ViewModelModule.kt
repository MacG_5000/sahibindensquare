package com.example.sahibindensquare.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.sahibindensquare.base.viewmodel.ViewModelFactory
import com.example.sahibindensquare.di.ViewModelKey
import com.example.sahibindensquare.modules.main.MainViewModel
import com.example.sahibindensquare.modules.search.detail.VenueDetailViewModel
import com.example.sahibindensquare.modules.search.list.SearchListViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

/**
 * Binds view models
 */
@Module
abstract class ViewModelModule {

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    abstract fun bindMainViewModel(viewModel: MainViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SearchListViewModel::class)
    abstract fun bindSearchListViewModel(viewModel: SearchListViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(VenueDetailViewModel::class)
    abstract fun bindVenueDetailViewModel(viewModel: VenueDetailViewModel): ViewModel

}