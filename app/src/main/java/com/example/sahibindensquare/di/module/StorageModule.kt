package com.example.sahibindensquare.di.module

import android.content.Context
import com.example.sahibindensquare.constants.Config
import com.example.sahibindensquare.data.SecurePreferences
import com.example.sahibindensquare.data.UserManager
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class StorageModule {

    @Singleton
    @Provides
    fun provideSecurePreferences(context: Context): SecurePreferences =
            SecurePreferences(context, Config.SECURE_PREFS, Config.X , true)

    @Singleton
    @Provides
    fun provideUserManager(securePreferences: SecurePreferences): UserManager = UserManager(securePreferences)
}