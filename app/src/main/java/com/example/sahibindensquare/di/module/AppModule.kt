package com.example.sahibindensquare.di.module

import android.content.Context
import com.example.sahibindensquare.AppControl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(includes = [StorageModule::class, ViewModelModule::class, NetworkModule::class])
class AppModule {

    @Singleton
    @Provides
    fun provideContext(application: AppControl): Context = application.applicationContext
}