package com.example.sahibindensquare.di.module

import com.example.sahibindensquare.di.FragmentScope
import com.example.sahibindensquare.modules.login.LoginFragment
import com.example.sahibindensquare.modules.main.MainActivity
import com.example.sahibindensquare.modules.search.detail.VenueDetailFragment
import com.example.sahibindensquare.modules.search.list.SearchListFragment
import com.example.sahibindensquare.modules.search.other.OtherFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Binds activities and fragments
 */
@Module
abstract class ViewBuilderModule {

    @ContributesAndroidInjector
    internal abstract fun bindMainActivity(): MainActivity

    @FragmentScope
    @ContributesAndroidInjector
    internal abstract fun bindLoginFragment(): LoginFragment

    @FragmentScope
    @ContributesAndroidInjector
    internal abstract fun bindOtherFragment(): OtherFragment

    @FragmentScope
    @ContributesAndroidInjector
    internal abstract fun bindSearchListFragment(): SearchListFragment

    @FragmentScope
    @ContributesAndroidInjector
    internal abstract fun bindDetailFragment(): VenueDetailFragment

}